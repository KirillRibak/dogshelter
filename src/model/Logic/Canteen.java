package model.Logic;

import model.dog.Dog;
import model.dog.DogMenu;

public class Canteen extends Logic implements FunctionalLogic{

    private DogMenu FOR_YONG = DogMenu.Pedigree_для_щенков;
    private DogMenu FOR_DOG = DogMenu.Pedigree_Vital;
    private DogMenu FOR_OLD = DogMenu.Happy_Dog_Senior;

    private double SICK_COEF = 1.5;

    public void eat(Dog dog) {

        if (dog.getAge() < 2) {
            if (dog.isSickness()) {
                double digestion = (FOR_YONG.getMass() * SICK_COEF) / super.getSickСoefficient();
                Double foodPoint = (100 * digestion) / FOR_YONG.getMass() * SICK_COEF;
                dog.setSatiety(foodPoint.intValue());
                getState(foodPoint.intValue(), dog);
            } else {
                double digestion = FOR_YONG.getMass() / super.getСoefficient();
                Double foodPoint = (100 * digestion) / FOR_YONG.getMass();
                dog.setSatiety(foodPoint.intValue());
                getState(foodPoint.intValue(), dog);
            }
        } else if (dog.getAge() < 8) {
            if (dog.isSickness()) {
                double digestion = (FOR_DOG.getMass() * SICK_COEF) / super.getSickСoefficient();
                Double foodPoint = (100 * digestion) / FOR_YONG.getMass() * SICK_COEF;
                dog.setSatiety(foodPoint.intValue());
                getState(foodPoint.intValue(), dog);
            } else {
                double digestion = FOR_DOG.getMass() / super.getСoefficient();
                Double foodPoint = (100 * digestion) / FOR_DOG.getMass();
                dog.setSatiety(foodPoint.intValue());
                getState(foodPoint.intValue(), dog);
            }
        } else {
            if (dog.isSickness()) {
                double digestion = (FOR_OLD.getMass() * SICK_COEF) / super.getSickСoefficient();
                Double foodPoint = (100 * digestion) / FOR_OLD.getMass() * SICK_COEF;
                dog.setSatiety(foodPoint.intValue());
                getState(foodPoint.intValue(), dog);
            } else {
                double digestion = FOR_OLD.getMass() / super.getСoefficient();
                Double foodPoint = (100 * digestion) / FOR_OLD.getMass();
                dog.setSatiety(foodPoint.intValue());
                getState(foodPoint.intValue(), dog);
            }
        }
    }

    @Override
    public void getState(int n, Dog dog) {
        print("Сытость: " + dog.getDogName() + " + " + n);
    }


}
