package model.Logic;

import model.dog.Dog;
import util.RandomValue;
import view.View;

public abstract class Logic {

    private View view;
    private RandomValue rnd;

    public Logic() {
        this.rnd = new RandomValue();
        this.view = new View();
    }

    public void print(String smth) {
        view.print(smth);
    }

    public int getRandomValue(int v) {
        return rnd.getRandomValue(v);
    }

    public int getRandimInRange(int min, int max) {
        return rnd.getRandimInRange(min, max);
    }

    public int getSickСoefficient() {
        return rnd.getSickСoefficient();
    }

    public int getСoefficient() {
        return rnd.getСoefficient();
    }
}
