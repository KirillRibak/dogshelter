package model.Logic;

import model.dog.Dog;

public class Hospital extends Logic implements FunctionalLogic {

    public void inspect(Dog dog) {
        if (dog.getHealth() < 50) {
            toTreat(dog);
        } else if (dog.getHealth() < 90) {
            toImprove(dog);
        } else {
            super.print(dog.getDogName() + "- Здоров!");
            setDiagnosis(dog);
        }
    }

    public void setDiagnosis(Dog dog) {
        if (dog.getHealth() < 50) {
            dog.setSickness(true);
        } else {
            dog.setSickness(false);
        }
    }

    public void toTreat(Dog dog) {
        if (dog.getAge() < 8) {
            int hillPoints = 0;
            super.print("" + dog.getDogName() + " мы тебя быстро на ноги поставим");
            if (dog.getSatiety() < 60) {
                hillPoints = super.getRandomValue(5) + 1;
            } else {
                hillPoints = super.getRandomValue(10) + 1;
            }
            dog.setHealth(dog.getHealth() + hillPoints);

            setDiagnosis(dog);
            getState(hillPoints, dog);
        } else {
            super.print("" + dog.getDogName() + " ничего,еще молодым фору дашь");
            int hillPoints = super.getRandomValue(6);

            dog.setHealth(dog.getHealth() + hillPoints);
            getState(hillPoints, dog);
            setDiagnosis(dog);
        }
    }

    public void toImprove(Dog dog) {
        super.print(dog.getDogName() + " попьешь вот эти витаминки");
        if (dog.getAge() < 8) {
            int hillPoints = super.getRandomValue(20);

            dog.setHealth(dog.getHealth() + hillPoints);
            getState(hillPoints, dog);
        } else {
            int hillPoints = super.getRandomValue(15);

            dog.setHealth(dog.getHealth() + hillPoints);
            getState(hillPoints, dog);
        }
        setDiagnosis(dog);
    }

    @Override
    public void getState(int n, Dog dog) {
        print("Здоровье " + "\t" + dog.getDogName() +"\t" + " поправилось на"+ "\t" + n + "\t"+"единиц");
    }
}
