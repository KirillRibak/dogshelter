package model.Logic;

import model.dog.Dog;

public class TrainingGround extends Logic implements FunctionalLogic {

    private void toTrain(Dog dog) {
        if (dog.getAge() < 2) {
            int trainPoint = super.getRandomValue(10);
            dog.setDressing(dog.getDressing() + trainPoint);
        } else {
            int trainPoint = super.getRandomValue(6);
            dog.setDressing(dog.getDressing() + trainPoint);
        }
    }

    public void filter(Dog dog) {
        if (dog.getAge() < 8 && dog.isSickness()) {
            super.print("С тобой, " + dog.getDogName() + " ,увидимся в другой раз");
        } else if (dog.getAge() < 8) {
            toTrain(dog);
        } else {
            super.print("Старого пса новым трюкам не научишь,правда " + dog.getDogName() + " ?");
        }
    }

    @Override
    public void getState(int n, Dog dog) {
        print(dog.getDogName() +"\t" + " обучился на" + n + "\t" + "единиц");
    }
}
