package model.Logic;

import controller.worckers.Trainer;
import model.dog.Dog;

public class PlaceOfWork extends Logic {

    private static final Trainer trainer = new Trainer();

    public void toWork(Dog dog) {
        if (dog.getDressing() < 30) {
            super.print(dog.getDogName() + ": отправился в спасательную службу" +
                    ",с работой справился так себе");
        } else if (dog.getDressing() < 50) {
            super.print(dog.getDogName() + ": отправился в спасательную службу" +
                    ",с работой справился неплохо");

        } else if (dog.getDressing() < 70) {
            super.print(dog.getDogName() + ": отправился в спасательную службу" +
                    ",с работой справился отлично");
        } else {
            super.print(dog.getDogName() + ": отправился в милицию службу" +
                    ",образцовый пес");

        }

    }

    public void sort(Dog dog) {
        if (dog.getAge() > 2 && dog.getAge() < 8) {
            isSeak(dog);
        }
    }

    private void isSeak(Dog dog) {
        if (dog.isSickness()) {
            print(dog.getDogName() + " - Поправляйся, дружок");
        } else {
            toWork(dog);
        }
    }

}
