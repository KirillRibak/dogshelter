package model.Logic;

import model.dog.Dog;

public interface FunctionalLogic {
    public abstract void getState(int n, Dog dog);
}
