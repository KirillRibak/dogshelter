package model.container;

import model.dog.Dog;
import view.View;

import java.util.ArrayList;
import java.util.List;

public class Shelter {

    private int MAX_SIZE = 30;

    private View view = new View();

    private List<Dog> shelter;

    public Shelter(){
        this.shelter = new ArrayList<>();
    }

    public boolean isFull() {
        if (shelter.size() < MAX_SIZE) {
            return false;
        }
        return true;
    }

    public void addNewDog(Dog dog) {
        if (isFull()) {
            view.print("Our shelter is full");
        }
        shelter.add(dog);
    }

    public int getAllSpace() {
        return MAX_SIZE;
    }

    public int getFreeSpace() {
        return MAX_SIZE - shelter.size();
    }

    public int getDogValue() {
        return shelter.size();
    }

    public Dog getDog(int index){
        return shelter.get(index);
    }


    @Override
    public String toString() {
        String str = "";
        for(Dog dog :shelter){
            str+=dog.toString();
        }
        return "Наш приют это:"+"\n"+str;

    }
}
