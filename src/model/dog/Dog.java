package model.dog;

public class Dog {

    private DogName dogName;

    private int age;

    private int health;

    private int satiety;

    private int dressing;

    private boolean sickness;


    public Dog(DogName dogName, int age, int health, int satiety, int dressing) {
        this.dogName = dogName;
        this.age = age;
        this.health = health;
        this.satiety = satiety;
        this.dressing = dressing;
        this.sickness = false;
    }

    public boolean isSickness() {
        return sickness;
    }

    public void setSickness(boolean sickness) {
        this.sickness = sickness;
    }

    public DogName getDogName() {
        return dogName;
    }

    public void setDogName(DogName dogName) {
        this.dogName = dogName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getDressing() {
        return dressing;
    }

    public void setDressing(int dressing) {
        this.dressing = dressing;
    }

    @Override
    public String toString() {
        return "Собакен:" +"\n"+
                "Имя =" + "\t"+dogName +"\n"+
                "Возраст=" +"\t"+ age +"\n"+
                "Здоровье=" +"\t"+ health +"\n"+
                "Сытость=" + "\t"+satiety +"\n"+
                "Дрессированность =" + dressing + "%" +"\n";
    }
}
