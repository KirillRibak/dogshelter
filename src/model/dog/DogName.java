package model.dog;

import util.RandomValue;

public enum DogName {
    Арчи, Алекс, Амур, Алтaй, Альф, Алмаз, Атос, Барон, Бутч, Боня, Бим, Бадди, Барни, Балу, Барс,
    Вольт, Вальтер, Веня, Вольф, Вулкан, Вегас, Ватсон, Гром, Граф, Грэй, Гектор, Гуччи, Ганс,
    Джек, Дик, Джонни, Деймон, Дёма, Еврик, Ерошка, Енисей, Елай, Ермак, Елисей, Жорик, Жан, Жак, Жулик, Жоржик, Жофрей,
    Зевс, Зефир, Зак, Зидан, Зольд, Зик;

    private static RandomValue random = new RandomValue();

    public static DogName getSomeName() {
        int range = DogName.values().length;
        DogName[] dogs = DogName.values();
        return dogs[random.getRandomValue(range)-1];
    }

}
