package model.dog;

public enum DogMenu {
    Pedigree_для_щенков(300),Pedigree_Vital(500),Happy_Dog_Senior(400);

    private int mass;

    DogMenu(int m){
        this.mass=m;
    }

    public  int getMass() {
        return mass;
    }
}
