package controller;

import controller.worckers.*;
import model.container.Shelter;
import view.View;

public class Schedule {

    private Cleaner cleaner;
    private Employer employer;
    private Feeding feeding;
    private Trainer trainer;
    private Veterinarian veterinarian;
    private View view;

    public Schedule() {
        this.cleaner = new Cleaner();
        this.employer = new Employer();
        this.feeding = new Feeding();
        this.trainer = new Trainer();
        this.veterinarian = new Veterinarian();
        this.view = new View();
    }

    private void updateStatement(Shelter shelter) {
        for (int i = 0; i < shelter.getDogValue(); i++) {
            shelter.getDog(i).setSatiety(0);
        }
    }

    public void generateDay(Shelter shelter){
        updateStatement(shelter);
        view.print("Завтрак:"+"\n");
        feeding.filter(shelter);
        view.print("\n\n"+"Все на осмотр к ветеринару:");
        veterinarian.filter(shelter);
        view.print("\n\n"+"Кто то двигает на тренировочку:");
        trainer.filter(shelter);
        view.print("\n\n"+"А в это время идет уборочка:");
        cleaner.filter(shelter);
        view.print("\n\n"+"Кому то надо ж  в семье зарабатывать:");
        employer.filter(shelter);
        view.print("\n\n"+"Ну что ж , ужин ,господа");
        feeding.filter(shelter);
    }

}
