package controller;

import model.container.Shelter;
import model.dog.Dog;
import model.dog.DogName;
import util.RandomValue;
import view.View;

public class InitializationOfShelter {

    private RandomValue random = new RandomValue();
    private Shelter shelter = new Shelter();
    private View view = new View();

    public Shelter initShelter(){
        for (int i = 0; i< random.getRandimInRange(10,shelter.getAllSpace()); i++){
            Dog someDog = new Dog(DogName.getSomeName(),random.getRandimInRange(1,15),random.getRandimInRange(10,100),
                    random.getRandimInRange(0,100),random.getRandimInRange(10,90));
            shelter.addNewDog(someDog);
        }
        view.print(shelter.toString());
        return shelter;
    }
}
