package controller.worckers;

import model.Logic.Canteen;
import model.container.Shelter;

public class Feeding extends ShelterWorker {

    private final static Canteen CANTEEN = new Canteen();

    public Feeding() {
        super("Время подкрепиться - Кормящий");
    }

    @Override
    public void filter(Shelter shelter) {
        speech();
        for (int i = 0; i < shelter.getDogValue(); i++) {
            CANTEEN.eat(shelter.getDog(i));
        }
    }


}
