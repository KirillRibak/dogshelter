package controller.worckers;

import controller.worckers.ShelterWorker;
import model.Logic.TrainingGround;
import model.container.Shelter;


public class Trainer extends ShelterWorker {

    private final static TrainingGround TRAINING_GROUND = new TrainingGround();

    public Trainer() {
        super("Время потренироваться - Тренер");
    }

    @Override
    public void filter(Shelter shelter) {
        speech();
        for (int i = 0; i<shelter.getDogValue(); i++) {
            TRAINING_GROUND.filter(shelter.getDog(i));
        }
    }
}
