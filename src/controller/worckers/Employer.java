package controller.worckers;

import model.Logic.PlaceOfWork;
import model.container.Shelter;

public class Employer extends ShelterWorker {

    private static final PlaceOfWork PLACE_OF_WORK = new PlaceOfWork();

    public Employer() {
        super("Пора поработать - Работодатель");
    }

    @Override
    public void filter(Shelter shelter) {
        speech();
        for (int i = 0; i < shelter.getDogValue(); i++) {
            PLACE_OF_WORK.sort(shelter.getDog(i));
        }
    }
}
