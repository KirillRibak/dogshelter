package controller.worckers;

import model.Logic.Aviary;
import model.container.Shelter;

public class Cleaner extends ShelterWorker {

    private static final Aviary AVIARY = new Aviary();

    public Cleaner() {
        super("Время уборки - Уборщик");
    }

    @Override
    public void filter(Shelter shelter) {
        speech();
        for (int i = 0; i < shelter.getDogValue(); i++) {
            AVIARY.toClean(shelter.getDog(i));
        }
    }
}
