package controller.worckers;

import model.container.Shelter;
import view.View;

public abstract class ShelterWorker {

    private View view;

    private String speech;

    public ShelterWorker(String speech) {
        this.view = new View();
        this.speech = speech;
    }

    public void speech() {
        print(speech);
    }

    public void print(String smth) {
        view.print(smth);
    }

    abstract public void filter(Shelter shelter);
}
