package controller.worckers;

import model.Logic.Hospital;
import model.container.Shelter;

public class Veterinarian extends ShelterWorker {

    private final static Hospital HOSPITAL = new Hospital();

    public Veterinarian() {
        super("Ну давай ка тебя посмотрим - Ветеринар ");
    }

    @Override
    public void filter(Shelter shelter) {
        speech();
        for (int i = 0; i < shelter.getDogValue(); i++) {
            HOSPITAL.inspect(shelter.getDog(i));
        }
    }

}
