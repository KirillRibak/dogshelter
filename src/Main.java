import controller.InitializationOfShelter;
import controller.Schedule;

public class Main {

    public static void main(String[] args) {
        InitializationOfShelter init = new InitializationOfShelter();
        Schedule schedule = new Schedule();

        schedule.generateDay(init.initShelter());
    }

}
