package util;

import java.util.Random;

public class RandomValue {

    private Random rnd = new Random();

    public int getRandomValue(int v) {
        return rnd.nextInt(v)+1;
    }

    public int getRandimInRange(int min, int max) {
        return rnd.nextInt(max - min) + min;
    }

    public int getSickСoefficient() {
        return rnd.nextInt(30) + 1;
    }

    public int getСoefficient() {
        return rnd.nextInt(7) + 1;
    }
}
